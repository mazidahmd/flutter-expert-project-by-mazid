import 'package:core/presentation/bloc/watchlist/watchlist_tvs/watchlist_tvs_bloc.dart';
import 'package:core/presentation/widgets/tv_card_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import '../../core.dart';

class WatchlistTvsPage extends StatefulWidget {
  static const ROUTE_NAME = '/watchlist-tv';

  @override
  _WatchlistTvsPageState createState() => _WatchlistTvsPageState();
}

class _WatchlistTvsPageState extends State<WatchlistTvsPage> with RouteAware {
  @override
  void initState() {
    super.initState();
    Future.microtask(
        () => context.read<WatchlistTvsBloc>().add(FetchWatchlistTvs()));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  void didPopNext() {
    context.read<WatchlistTvsBloc>().add(FetchWatchlistTvs());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WatchlistTvsBloc, WatchlistTvsState>(
      builder: (context, state) {
        if (state is WatchlistTvsLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is WatchlistTvsLoaded) {
          return (state.tvs.length > 0)
              ? ListView.builder(
                  itemBuilder: (context, index) {
                    final tv = state.tvs[index];
                    return TvCard(tv);
                  },
                  itemCount: state.tvs.length,
                )
              : Center(
                  child: Text("Watchlist Empty"),
                );
        } else if (state is WatchlistTvsError) {
          return Center(
            key: Key('error_message'),
            child: Text(state.message),
          );
        }
        return Container();
      },
    );
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
