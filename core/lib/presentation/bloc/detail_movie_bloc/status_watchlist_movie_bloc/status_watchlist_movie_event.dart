part of 'status_watchlist_movie_bloc.dart';

abstract class StatusWatchlistMovieEvent extends Equatable {
  const StatusWatchlistMovieEvent();

  @override
  List<Object> get props => [];
}

class FetchStatusWatchlistMovie extends StatusWatchlistMovieEvent {
  final int id;

  FetchStatusWatchlistMovie(this.id);
}

class AddToWatchlist extends StatusWatchlistMovieEvent {
  final MovieDetail movie;

  AddToWatchlist(this.movie);
}

class RemoveFromWatchlist extends StatusWatchlistMovieEvent {
  final MovieDetail movie;

  RemoveFromWatchlist(this.movie);
}
