import 'package:core/data/models/genre_model.dart';
import 'package:core/data/models/movie_detail_model.dart';
import 'package:core/domain/entities/genre.dart';
import 'package:core/domain/entities/movie_detail.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final tMovieDetailModel = MovieDetailResponse(
    budget: 100000,
    homepage: "homepage",
    imdbId: "A1414",
    originalLanguage: "en",
    popularity: 100,
    revenue: 95438582,
    status: "true",
    tagline: "tagline",
    video: false,
    adult: false,
    backdropPath: 'backdropPath',
    genres: [GenreModel(id: 1, name: 'Action')],
    id: 1,
    originalTitle: 'originalTitle',
    overview: 'overview',
    posterPath: 'posterPath',
    releaseDate: 'releaseDate',
    runtime: 120,
    title: 'title',
    voteAverage: 1,
    voteCount: 1,
  );

  final tMovieDetail = MovieDetail(
    adult: false,
    backdropPath: 'backdropPath',
    genres: [Genre(id: 1, name: 'Action')],
    id: 1,
    originalTitle: 'originalTitle',
    overview: 'overview',
    posterPath: 'posterPath',
    releaseDate: 'releaseDate',
    runtime: 120,
    title: 'title',
    voteAverage: 1,
    voteCount: 1,
  );

  test('should be a subclass of Movie entity', () async {
    final result = tMovieDetailModel.toEntity();
    expect(result, tMovieDetail);
  });

  group('toJson', () {
    test('should return a JSON map containing proper data', () async {
      // arrange

      // act
      final result = tMovieDetailModel.toJson();
      // assert
      final expectedJsonMap = {
        "budget": 100000,
        "homepage": "homepage",
        "imdb_id": "A1414",
        "original_language": "en",
        "popularity": 100,
        "revenue": 95438582,
        "status": "true",
        "tagline": "tagline",
        "video": false,
        "adult": false,
        "backdrop_path": 'backdropPath',
        "genres": [
          {"id": 1, "name": 'Action'}
        ],
        "id": 1,
        "original_title": 'originalTitle',
        "overview": 'overview',
        "poster_path": 'posterPath',
        "release_date": 'releaseDate',
        "runtime": 120,
        "title": 'title',
        "vote_average": 1,
        "vote_count": 1,
      };
      expect(result, expectedJsonMap);
    });
  });
}
