import 'package:core/core.dart';
import 'package:http/http.dart' as http;
import 'package:http_certificate_pinning/http_certificate_pinning.dart';

class NetworkUtils {
  static final List<String> serverFingerPrint = [
    'E0:F6:90:BB:E9:D9:51:8A:42:A6:84:02:D8:7F:44:85:EC:38:F8:A3:D3:4D:90:5F:EE:F7:2C:D7:FB:B9:52:08',
    'B4:22:6E:34:DF:58:4C:CC:CB:C1:D7:08:25:72:9C:4F:81:6F:83:BA'
  ];

  Future checkCertificate(String url, Map<String, String> headers) async {
    try {
      final secure = await HttpCertificatePinning.check(
          serverURL: url,
          headerHttp: headers,
          sha: SHA.SHA256,
          allowedSHAFingerprints: serverFingerPrint,
          timeout: 50);

      if (secure.contains("CONNECTION_SECURE")) {
        return true;
      } else {
        throw ServerException();
      }
    } catch (e) {
      throw ServerException();
    }
  }

  Future<http.Response> requestGet(http.Client client, String url) async {
    await checkCertificate(url, {});

    var response = client.get(Uri.parse(url));
    return response;
  }
}
