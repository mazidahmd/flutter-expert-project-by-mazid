import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/usecases/get_top_rated_tvs.dart';
import 'package:equatable/equatable.dart';

part 'top_rated_tvs_event.dart';
part 'top_rated_tvs_state.dart';

class TopRatedTvsBloc extends Bloc<TopRatedTvsEvent, TopRatedTvsState> {
  final GetTopRatedTvs _getTopRatedTvs;
  TopRatedTvsBloc(this._getTopRatedTvs) : super(TopRatedTvsEmpty());

  @override
  Stream<TopRatedTvsState> mapEventToState(
    TopRatedTvsEvent event,
  ) async* {
    if (event is FetchTopRatedTvsEvent) {
      yield TopRatedTvsLoading();
      final result = await _getTopRatedTvs.execute();
      yield* result.fold((failed) async* {
        yield TopRatedTvsError(failed.message);
      }, (data) async* {
        yield TopRatedTvsLoaded(data);
      });
    }
  }
}
