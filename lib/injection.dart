import 'package:core/core.dart';
import 'package:core/utils/network.dart';
import 'package:http/http.dart' as http;
import 'package:get_it/get_it.dart';
import 'package:search/search.dart';

final locator = GetIt.instance;

void init() {
  // Bloc
  locator.registerFactory(
    () => SearchBloc(locator(), locator()),
  );

  locator.registerFactory(
    () => TopRatedMoviesBloc(locator()),
  );

  locator.registerFactory(
    () => PopularMoviesBloc(locator()),
  );

  locator.registerFactory(
    () => NowPlayingMoviesBloc(locator()),
  );

  locator.registerFactory(
    () => DetailMovieBloc(locator(), locator()),
  );

  locator.registerFactory(
    () => StatusWatchlistMovieBloc(locator(), locator(), locator()),
  );

  locator.registerFactory(
    () => TopRatedTvsBloc(locator()),
  );

  locator.registerFactory(
    () => PopularTvsBloc(locator()),
  );

  locator.registerFactory(
    () => NowPlayingTvsBloc(locator()),
  );

  locator.registerFactory(
    () => DetailTvBloc(locator(), locator()),
  );

  locator.registerFactory(
    () => StatusWatchlistTvBloc(locator(), locator(), locator()),
  );

  locator.registerFactory(
    () => WatchlistMoviesBloc(locator()),
  );

  locator.registerFactory(
    () => WatchlistTvsBloc(locator()),
  );

  // use case
  locator.registerLazySingleton(() => GetNowPlayingMovies(locator()));
  locator.registerLazySingleton(() => GetPopularMovies(locator()));
  locator.registerLazySingleton(() => GetTopRatedMovies(locator()));
  locator.registerLazySingleton(() => GetMovieDetail(locator()));
  locator.registerLazySingleton(() => GetMovieRecommendations(locator()));
  locator.registerLazySingleton(() => SearchMovies(locator()));
  locator.registerLazySingleton(() => GetWatchListStatus(locator()));
  locator.registerLazySingleton(() => SaveWatchlist(locator()));
  locator.registerLazySingleton(() => RemoveWatchlist(locator()));
  locator.registerLazySingleton(() => GetWatchlistMovies(locator()));
  locator.registerLazySingleton(() => GetNowPlayingTvs(locator()));
  locator.registerLazySingleton(() => GetPopularTvs(locator()));
  locator.registerLazySingleton(() => GetTopRatedTvs(locator()));
  locator.registerLazySingleton(() => GetTvDetail(locator()));
  locator.registerLazySingleton(() => GetTvRecommendations(locator()));
  locator.registerLazySingleton(() => SearchTvs(locator()));
  locator.registerLazySingleton(() => GetWatchListStatusTv(locator()));
  locator.registerLazySingleton(() => SaveWatchlistTv(locator()));
  locator.registerLazySingleton(() => RemoveWatchlistTv(locator()));
  locator.registerLazySingleton(() => GetWatchlistTvs(locator()));

  // repository
  locator.registerLazySingleton<MovieRepository>(
    () => MovieRepositoryImpl(
      remoteDataSource: locator(),
      localDataSource: locator(),
    ),
  );
  locator.registerLazySingleton<TvRepository>(
    () => TvRepositoryImpl(
      remoteDataSource: locator(),
      localDataSource: locator(),
    ),
  );

  // data sources
  locator.registerLazySingleton<MovieRemoteDataSource>(
      () => MovieRemoteDataSourceImpl(client: locator(), network: locator()));
  locator.registerLazySingleton<MovieLocalDataSource>(
      () => MovieLocalDataSourceImpl(databaseHelper: locator()));
  locator.registerLazySingleton<TvRemoteDataSource>(
      () => TvRemoteDataSourceImpl(client: locator(), network: locator()));
  locator.registerLazySingleton<TvLocalDataSource>(
      () => TvLocalDataSourceImpl(databaseHelper: locator()));

  // helper
  locator.registerLazySingleton<DatabaseHelper>(() => DatabaseHelper());

  // external
  locator.registerLazySingleton(() => http.Client());
  locator.registerLazySingleton(() => NetworkUtils());
}
