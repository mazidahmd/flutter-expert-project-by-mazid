part of 'detail_tv_bloc.dart';

abstract class DetailTvState extends Equatable {
  const DetailTvState();

  @override
  List<Object> get props => [];
}

class DetailTvInitial extends DetailTvState {}

class DetailTvEmpty extends DetailTvState {}

class DetailTvLoading extends DetailTvState {}

class DetailTvLoaded extends DetailTvState {
  final TvDetail tv;
  final List<Tv> recommendation;

  DetailTvLoaded({required this.tv, required this.recommendation});
}

class DetailTvError extends DetailTvState {
  final String message;

  DetailTvError(this.message);
}
