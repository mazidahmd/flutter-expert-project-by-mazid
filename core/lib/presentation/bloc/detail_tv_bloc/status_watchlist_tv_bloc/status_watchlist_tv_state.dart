part of 'status_watchlist_tv_bloc.dart';

abstract class StatusWatchlistTvState extends Equatable {
  const StatusWatchlistTvState();

  @override
  List<Object> get props => [];
}

class StatusWatchlistTvInitial extends StatusWatchlistTvState {}

class StatusWatchlistTvEmpty extends StatusWatchlistTvState {}

class StatusWatchlistTvLoading extends StatusWatchlistTvState {}

class StatusWatchlistTvLoaded extends StatusWatchlistTvState {
  final bool isWishlisted;
  final String? message;

  StatusWatchlistTvLoaded({required this.isWishlisted, this.message});
}

class StatusWatchlistTvError extends StatusWatchlistTvState {
  final String message;

  StatusWatchlistTvError(this.message);
}
