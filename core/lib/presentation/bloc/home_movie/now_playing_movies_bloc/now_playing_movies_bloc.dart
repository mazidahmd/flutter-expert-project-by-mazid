import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/domain/usecases/get_now_playing_movies.dart';
import 'package:equatable/equatable.dart';

part 'now_playing_movies_event.dart';
part 'now_playing_movies_state.dart';

class NowPlayingMoviesBloc
    extends Bloc<NowPlayingMoviesEvent, NowPlayingMoviesState> {
  final GetNowPlayingMovies _getNowPlayingMovies;
  NowPlayingMoviesBloc(this._getNowPlayingMovies)
      : super(NowPlayingMoviesEmpty());

  @override
  Stream<NowPlayingMoviesState> mapEventToState(
    NowPlayingMoviesEvent event,
  ) async* {
    if (event is FetchNowPlayingMoviesEvent) {
      yield NowPlayingMoviesLoading();
      final result = await _getNowPlayingMovies.execute();
      yield* result.fold((failed) async* {
        yield NowPlayingMoviesError(failed.message);
      }, (data) async* {
        yield NowPlayingMoviesLoaded(data);
      });
    }
  }
}
