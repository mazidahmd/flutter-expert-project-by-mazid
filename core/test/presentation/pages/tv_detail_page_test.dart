import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/presentation/pages/tv_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../dummy_data/dummy_objects.dart';

class MockDetailTvBloc extends MockBloc<DetailTvEvent, DetailTvState>
    implements DetailTvBloc {}

class MockDetailTvEvent extends Fake implements DetailTvEvent {}

class MockDetailTvState extends Fake implements DetailTvState {}

class MockStatusWatchlistTvBloc
    extends MockBloc<StatusWatchlistTvEvent, StatusWatchlistTvState>
    implements StatusWatchlistTvBloc {}

class MockStatusWatchlistTvEvent extends Fake
    implements StatusWatchlistTvEvent {}

class MockStatusWatchlistTvState extends Fake
    implements StatusWatchlistTvState {}

void main() {
  late MockDetailTvBloc mockDetailBloc;
  late MockStatusWatchlistTvBloc mockStatusBloc;

  setUp(() {
    mockDetailBloc = MockDetailTvBloc();
    mockStatusBloc = MockStatusWatchlistTvBloc();
  });

  setUpAll(() {
    registerFallbackValue(MockDetailTvEvent());
    registerFallbackValue(MockDetailTvState());
    registerFallbackValue(MockStatusWatchlistTvEvent());
    registerFallbackValue(MockStatusWatchlistTvState());
  });

  tearDown(() {
    mockStatusBloc.close();
    mockDetailBloc.close();
  });

  Widget _makeTestableWidget(Widget body) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<DetailTvBloc>(create: (context) => mockDetailBloc),
        BlocProvider<StatusWatchlistTvBloc>(create: (context) => mockStatusBloc)
      ],
      child: MaterialApp(
        home: body,
      ),
    );
  }

  testWidgets(
      'Watchlist button should display add icon when Tv not added to watchlist',
      (WidgetTester tester) async {
    when(() => mockDetailBloc.add(FetchDetailTv(1)))
        .thenAnswer((_) async => {});
    when(() => mockDetailBloc.state).thenAnswer(
        (_) => DetailTvLoaded(tv: testTvDetail, recommendation: <Tv>[]));
    when(() => mockStatusBloc.add(FetchStatusWatchlistTv(1)))
        .thenAnswer((_) async => {});
    when(() => mockStatusBloc.state)
        .thenAnswer((_) => StatusWatchlistTvLoaded(isWishlisted: false));

    final watchlistButtonIcon = find.byIcon(Icons.add);

    await tester.pumpWidget(_makeTestableWidget(TvDetailPage(id: 1)));

    expect(watchlistButtonIcon, findsOneWidget);
  });

  testWidgets(
      'Watchlist button should dispay check icon when Tv is added to wathclist',
      (WidgetTester tester) async {
    when(() => mockDetailBloc.state).thenAnswer(
        (_) => DetailTvLoaded(tv: testTvDetail, recommendation: <Tv>[]));
    when(() => mockStatusBloc.state)
        .thenAnswer((_) => StatusWatchlistTvLoaded(isWishlisted: true));

    final watchlistButtonIcon = find.byIcon(Icons.check);

    await tester.pumpWidget(_makeTestableWidget(TvDetailPage(id: 1)));

    expect(watchlistButtonIcon, findsOneWidget);
  });

  // testWidgets(
  //     'Watchlist button should display Snackbar when added to watchlist',
  //     (WidgetTester tester) async {
  //   when(mockBloc.tvState).thenReturn(RequestState.Loaded);
  //   when(mockBloc.tv).thenReturn(testTvDetail);
  //   when(mockBloc.recommendationState).thenReturn(RequestState.Loaded);
  //   when(mockBloc.tvRecommendations).thenReturn(<Tv>[]);
  //   when(mockBloc.isAddedToWatchlist).thenReturn(false);
  //   when(mockBloc.watchlistMessage).thenReturn('Added to Watchlist');

  //   final watchlistButton = find.byType(ElevatedButton);

  //   await tester.pumpWidget(_makeTestableWidget(TvDetailPage(id: 1)));

  //   expect(find.byIcon(Icons.add), findsOneWidget);

  //   await tester.tap(watchlistButton);
  //   await tester.pump();

  //   expect(find.byType(SnackBar), findsOneWidget);
  //   expect(find.text('Added to Watchlist'), findsOneWidget);
  // });

  // testWidgets(
  //     'Watchlist button should display AlertDialog when add to watchlist failed',
  //     (WidgetTester tester) async {
  //   when(mockBloc.tvState).thenReturn(RequestState.Loaded);
  //   when(mockBloc.tv).thenReturn(testTvDetail);
  //   when(mockBloc.recommendationState).thenReturn(RequestState.Loaded);
  //   when(mockBloc.tvRecommendations).thenReturn(<Tv>[]);
  //   when(mockBloc.isAddedToWatchlist).thenReturn(false);
  //   when(mockBloc.watchlistMessage).thenReturn('Failed');

  //   final watchlistButton = find.byType(ElevatedButton);

  //   await tester.pumpWidget(_makeTestableWidget(TvDetailPage(id: 1)));

  //   expect(find.byIcon(Icons.add), findsOneWidget);

  //   await tester.tap(watchlistButton);
  //   await tester.pump();

  //   expect(find.byType(AlertDialog), findsOneWidget);
  //   expect(find.text('Failed'), findsOneWidget);
  // });
}
