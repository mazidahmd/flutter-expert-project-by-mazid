part of 'now_playing_tvs_bloc.dart';

abstract class NowPlayingTvsState extends Equatable {
  @override
  List<Object> get props => [];
}

class NowPlayingTvsEmpty extends NowPlayingTvsState {}

class NowPlayingTvsLoading extends NowPlayingTvsState {}

class NowPlayingTvsLoaded extends NowPlayingTvsState {
  final List<Tv> tvs;

  NowPlayingTvsLoaded(this.tvs);
}

class NowPlayingTvsError extends NowPlayingTvsState {
  final String message;

  NowPlayingTvsError(this.message);
}
