import 'package:bloc_test/bloc_test.dart';
import 'package:core/presentation/bloc/home_movie/popular_tvs_bloc/popular_tvs_bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/presentation/pages/popular_tvs_page.dart';
import 'package:core/presentation/widgets/tv_card_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockPopularTvsBloc extends MockBloc<PopularTvsEvent, PopularTvsState>
    implements PopularTvsBloc {}

class MockPopularTvsEvent extends Fake implements PopularTvsEvent {}

class MockPopularTvsState extends Fake implements PopularTvsState {}

void main() {
  late MockPopularTvsBloc mockBloc;

  setUpAll(() {
    registerFallbackValue(MockPopularTvsEvent());
    registerFallbackValue(MockPopularTvsState());
  });

  setUp(() {
    mockBloc = MockPopularTvsBloc();
  });

  tearDown(() {
    mockBloc.close();
  });

  Widget _makeTestableWidget(Widget body) {
    return BlocProvider<PopularTvsBloc>(
      create: (context) => mockBloc,
      child: MaterialApp(
        home: body,
      ),
    );
  }

  testWidgets('Page should display container when empty',
      (WidgetTester tester) async {
    when(() => mockBloc.state).thenAnswer((_) => PopularTvsEmpty());

    final containerFinder = find.byType(Container);

    await tester.pumpWidget(_makeTestableWidget(PopularTvsPage()));

    expect(containerFinder, findsOneWidget);
  });

  testWidgets('Page should display center progress bar when loading',
      (WidgetTester tester) async {
    when(() => mockBloc.state).thenAnswer((_) => PopularTvsLoading());

    final progressBarFinder = find.byType(CircularProgressIndicator);
    final centerFinder = find.byType(Center);

    await tester.pumpWidget(_makeTestableWidget(PopularTvsPage()));

    expect(centerFinder, findsOneWidget);
    expect(progressBarFinder, findsOneWidget);
  });

  testWidgets('Page should display ListView when data is loaded',
      (WidgetTester tester) async {
    when(() => mockBloc.state).thenAnswer((_) => PopularTvsLoaded(<Tv>[
          Tv(
              backdropPath: "backdropPath",
              firstAirDate: DateTime.now(),
              genreIds: [1],
              id: 1,
              name: "tv",
              originCountry: ["en"],
              originalLanguage: "en",
              originalName: "tv",
              overview: "overview",
              popularity: 134,
              posterPath: "posterPath",
              voteAverage: 4.1,
              voteCount: 125)
        ]));

    final listViewFinder = find.byType(ListView);
    final tvCardFinder = find.byType(TvCard);

    await tester.pumpWidget(_makeTestableWidget(PopularTvsPage()));

    expect(listViewFinder, findsOneWidget);
    expect(tvCardFinder, findsOneWidget);
  });

  testWidgets('Page should display text with message when Error',
      (WidgetTester tester) async {
    when(() => mockBloc.state).thenReturn(PopularTvsError('Error message'));

    final textFinder = find.byKey(Key('error_message'));

    await tester.pumpWidget(_makeTestableWidget(PopularTvsPage()));

    expect(textFinder, findsOneWidget);
  });
}
