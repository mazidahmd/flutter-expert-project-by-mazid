import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'now_playing_tvs_bloc_test.mocks.dart';

@GenerateMocks([GetNowPlayingTvs])
void main() {
  late NowPlayingTvsBloc nowPlayingTvsBloc;
  late MockGetNowPlayingTvs mockGetNowPlayingTvs;

  setUp(() {
    mockGetNowPlayingTvs = MockGetNowPlayingTvs();
    nowPlayingTvsBloc = NowPlayingTvsBloc(mockGetNowPlayingTvs);
  });

  tearDown(() {
    nowPlayingTvsBloc.close();
  });

  test('initial nowPlayingTvsBloc state should be initial', () {
    expect(nowPlayingTvsBloc.state, NowPlayingTvsEmpty());
  });

  blocTest<NowPlayingTvsBloc, NowPlayingTvsState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetNowPlayingTvs.execute())
          .thenAnswer((_) async => Right(<Tv>[]));
      return nowPlayingTvsBloc;
    },
    act: (bloc) => bloc.add(FetchNowPlayingTvsEvent()),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      NowPlayingTvsLoading(),
      NowPlayingTvsLoaded(<Tv>[]),
    ],
    verify: (bloc) {
      verify(mockGetNowPlayingTvs.execute());
    },
  );

  blocTest<NowPlayingTvsBloc, NowPlayingTvsState>(
    'Should emit [Loading, Error] when data is gotten error',
    build: () {
      when(mockGetNowPlayingTvs.execute())
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return nowPlayingTvsBloc;
    },
    act: (bloc) => bloc.add(FetchNowPlayingTvsEvent()),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      NowPlayingTvsLoading(),
      NowPlayingTvsError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockGetNowPlayingTvs.execute());
    },
  );
}
