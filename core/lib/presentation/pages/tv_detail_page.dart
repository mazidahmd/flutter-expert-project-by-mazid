import 'package:cached_network_image/cached_network_image.dart';
import 'package:core/domain/entities/genre.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/entities/tv_detail.dart';
import 'package:core/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../core.dart';

class TvDetailPage extends StatefulWidget {
  final int id;
  TvDetailPage({required this.id});

  @override
  _TvDetailPageState createState() => _TvDetailPageState();
}

class _TvDetailPageState extends State<TvDetailPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<DetailTvBloc>().add(FetchDetailTv(widget.id));
      context
          .read<StatusWatchlistTvBloc>()
          .add(FetchStatusWatchlistTv(widget.id));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<DetailTvBloc, DetailTvState>(
        builder: (context, state) {
          if (state is DetailTvLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is DetailTvLoaded) {
            final tv = state.tv;
            return SafeArea(
              child: DetailContentTv(tv, state.recommendation),
            );
          } else if (state is DetailTvError) {
            return Text(state.message);
          }
          return Container();
        },
      ),
    );
  }
}

class DetailContentTv extends StatelessWidget {
  final TvDetail tv;
  final List<Tv> recommendations;

  DetailContentTv(this.tv, this.recommendations);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        CachedNetworkImage(
          imageUrl: 'https://image.tmdb.org/t/p/w500${tv.posterPath}',
          width: screenWidth,
          placeholder: (context, url) => Center(
            child: CircularProgressIndicator(),
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
        Container(
          margin: const EdgeInsets.only(top: 48 + 8),
          child: DraggableScrollableSheet(
            builder: (context, scrollController) {
              return Container(
                decoration: BoxDecoration(
                  color: kRichBlack,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
                ),
                padding: const EdgeInsets.only(
                  left: 16,
                  top: 16,
                  right: 16,
                ),
                child: Stack(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 16),
                      child: SingleChildScrollView(
                        controller: scrollController,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              tv.name ?? "-",
                              style: kHeading5,
                            ),
                            BlocConsumer<StatusWatchlistTvBloc,
                                StatusWatchlistTvState>(
                              listener: (context, state) {
                                if (state is StatusWatchlistTvLoaded) {
                                  final message = state.message;
                                  if (message != null)
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(content: Text(message)));
                                }
                                if (state is StatusWatchlistTvError) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          content: Text(state.message),
                                        );
                                      });
                                }
                              },
                              builder: (context, state) {
                                if (state is StatusWatchlistTvLoaded) {
                                  return ElevatedButton(
                                    onPressed: () async {
                                      if (!state.isWishlisted) {
                                        context
                                            .read<StatusWatchlistTvBloc>()
                                            .add(AddToWatchlistTv(tv));
                                      } else {
                                        context
                                            .read<StatusWatchlistTvBloc>()
                                            .add(RemoveFromWatchlistTv(tv));
                                      }
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        state.isWishlisted
                                            ? Icon(Icons.check)
                                            : Icon(Icons.add),
                                        Text('Watchlist'),
                                      ],
                                    ),
                                  );
                                } else if (state is StatusWatchlistTvLoading) {
                                  return CircularProgressIndicator();
                                }
                                return Container();
                              },
                            ),
                            Text(
                              _showGenres(tv.genres),
                            ),
                            if (tv.episodeRunTime != null &&
                                tv.episodeRunTime!.length > 0)
                              Text(
                                _showDuration(tv.episodeRunTime?[0]),
                              ),
                            Row(
                              children: [
                                RatingBarIndicator(
                                  rating: tv.voteAverage != null
                                      ? tv.voteAverage! / 2
                                      : 0,
                                  itemCount: 5,
                                  itemBuilder: (context, index) => const Icon(
                                    Icons.star,
                                    color: kMikadoYellow,
                                  ),
                                  itemSize: 24,
                                ),
                                Text('${tv.voteAverage}')
                              ],
                            ),
                            SizedBox(height: 16),
                            if (tv.seasons != null && tv.seasons!.length > 0)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Seasons',
                                    style: kHeading6,
                                  ),
                                  SizedBox(height: 16),
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemCount: tv.seasons?.length,
                                      itemBuilder: (context, idx) {
                                        var _season = tv.seasons![idx];
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  if (_season.posterPath !=
                                                      null)
                                                    Container(
                                                      width: 80,
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            "https://image.tmdb.org/t/p/w500${_season.posterPath}",
                                                      ),
                                                    ),
                                                  SizedBox(width: 20),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Text(
                                                              "${_season.name}",
                                                              style: kSubtitle,
                                                            ),
                                                            SizedBox(width: 15),
                                                            Row(
                                                              children: [
                                                                Icon(Icons
                                                                    .playlist_add_check_rounded),
                                                                SizedBox(
                                                                    width: 5),
                                                                Text(
                                                                  "${_season.episodeCount} Episodes",
                                                                  style:
                                                                      kBodyText,
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                        Text(
                                                          "${_season.overview}",
                                                          style: kBodyText,
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        );
                                      }),
                                ],
                              ),
                            SizedBox(height: 16),
                            Text(
                              'Overview',
                              style: kHeading6,
                            ),
                            Text(
                              tv.overview ?? "-",
                            ),
                            SizedBox(height: 16),
                            Text(
                              'Recommendations',
                              style: kHeading6,
                            ),
                            BlocBuilder<DetailTvBloc, DetailTvState>(
                              builder: (context, state) {
                                if (state is DetailTvLoading) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                } else if (state is DetailTvError) {
                                  return Text(state.message);
                                } else if (state is DetailTvLoaded) {
                                  return Container(
                                    height: 150,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {
                                        final tv = recommendations[index];
                                        return Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: InkWell(
                                            onTap: () {
                                              Navigator.pushReplacementNamed(
                                                context,
                                                TV_DETAIL_ROUTE,
                                                arguments: tv.id,
                                              );
                                            },
                                            child: ClipRRect(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(8),
                                              ),
                                              child: CachedNetworkImage(
                                                imageUrl:
                                                    'https://image.tmdb.org/t/p/w500${tv.posterPath}',
                                                placeholder: (context, url) =>
                                                    const Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Icon(Icons.error),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                      itemCount: recommendations.length,
                                    ),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        color: Colors.white,
                        height: 4,
                        width: 48,
                      ),
                    ),
                  ],
                ),
              );
            },
            // initialChildSize: 0.5,
            minChildSize: 0.25,
            // maxChildSize: 1.0,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundColor: kRichBlack,
            foregroundColor: Colors.white,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        )
      ],
    );
  }

  String _showGenres(List<Genre>? genres) {
    String result = '';

    if (genres == null) return "-";

    for (var genre in genres) {
      result += genre.name + ', ';
    }

    if (result.isEmpty) {
      return result;
    }

    return result.substring(0, result.length - 2);
  }

  String _showDuration(int? runtime) {
    if (runtime == null) return "-";

    final int hours = runtime ~/ 60;
    final int minutes = runtime % 60;

    if (hours > 0) {
      return '${hours}h ${minutes}m';
    } else {
      return '${minutes}m';
    }
  }
}
