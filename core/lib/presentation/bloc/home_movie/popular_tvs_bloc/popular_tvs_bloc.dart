import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/usecases/get_popular_tvs.dart';
import 'package:equatable/equatable.dart';

part 'popular_tvs_event.dart';
part 'popular_tvs_state.dart';

class PopularTvsBloc extends Bloc<PopularTvsEvent, PopularTvsState> {
  final GetPopularTvs _getPopularTvs;
  PopularTvsBloc(this._getPopularTvs) : super(PopularTvsEmpty());

  @override
  Stream<PopularTvsState> mapEventToState(
    PopularTvsEvent event,
  ) async* {
    if (event is FetchPopularTvsEvent) {
      yield PopularTvsLoading();
      final result = await _getPopularTvs.execute();
      yield* result.fold((failed) async* {
        yield PopularTvsError(failed.message);
      }, (data) async* {
        yield PopularTvsLoaded(data);
      });
    }
  }
}
