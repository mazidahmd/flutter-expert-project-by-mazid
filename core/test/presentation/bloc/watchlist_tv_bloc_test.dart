import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'watchlist_tv_bloc_test.mocks.dart';

@GenerateMocks([GetWatchlistTvs])
void main() {
  late WatchlistTvsBloc watchlistTvsBloc;
  late MockGetWatchlistTvs mockGetWatchlistTvs;

  setUp(() {
    mockGetWatchlistTvs = MockGetWatchlistTvs();
    watchlistTvsBloc = WatchlistTvsBloc(mockGetWatchlistTvs);
  });

  tearDown(() {
    watchlistTvsBloc.close();
  });

  test('initial statusWatchlistTv state should be initial', () {
    expect(watchlistTvsBloc.state, WatchlistTvsInitial());
  });

  blocTest<WatchlistTvsBloc, WatchlistTvsState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetWatchlistTvs.execute())
          .thenAnswer((_) async => Right(<Tv>[]));
      return watchlistTvsBloc;
    },
    act: (bloc) => bloc.add(FetchWatchlistTvs()),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      WatchlistTvsLoading(),
      WatchlistTvsLoaded(<Tv>[]),
    ],
    verify: (bloc) {
      verify(mockGetWatchlistTvs.execute());
    },
  );

  blocTest<WatchlistTvsBloc, WatchlistTvsState>(
    'Should emit [Loading, Error] when data is gotten error',
    build: () {
      when(mockGetWatchlistTvs.execute())
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return watchlistTvsBloc;
    },
    act: (bloc) => bloc.add(FetchWatchlistTvs()),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      WatchlistTvsLoading(),
      WatchlistTvsError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockGetWatchlistTvs.execute());
    },
  );
}
