part of 'status_watchlist_movie_bloc.dart';

abstract class StatusWatchlistMovieState extends Equatable {
  const StatusWatchlistMovieState();

  @override
  List<Object> get props => [];
}

class StatusWatchlistMovieInitial extends StatusWatchlistMovieState {}

class StatusWatchlistMovieEmpty extends StatusWatchlistMovieState {}

class StatusWatchlistMovieLoading extends StatusWatchlistMovieState {}

class StatusWatchlistMovieLoaded extends StatusWatchlistMovieState {
  final bool isWishlisted;
  final String? message;

  StatusWatchlistMovieLoaded({required this.isWishlisted, this.message});
}

class StatusWatchlistMovieError extends StatusWatchlistMovieState {
  final String message;

  StatusWatchlistMovieError(this.message);
}
