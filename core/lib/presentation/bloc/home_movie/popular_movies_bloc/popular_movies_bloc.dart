import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/domain/usecases/get_popular_movies.dart';
import 'package:equatable/equatable.dart';

part 'popular_movies_event.dart';
part 'popular_movies_state.dart';

class PopularMoviesBloc extends Bloc<PopularMoviesEvent, PopularMoviesState> {
  final GetPopularMovies _getPopularMovies;
  PopularMoviesBloc(this._getPopularMovies) : super(PopularMoviesEmpty());

  @override
  Stream<PopularMoviesState> mapEventToState(
    PopularMoviesEvent event,
  ) async* {
    if (event is FetchPopularMoviesEvent) {
      yield PopularMoviesLoading();
      final result = await _getPopularMovies.execute();
      yield* result.fold((failed) async* {
        yield PopularMoviesError(failed.message);
      }, (data) async* {
        yield PopularMoviesLoaded(data);
      });
    }
  }
}
