import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/entities/tv_detail.dart';
import 'package:core/domain/usecases/get_tv_detail.dart';
import 'package:core/domain/usecases/get_tv_recommendations.dart';
import 'package:equatable/equatable.dart';

part 'detail_tv_event.dart';
part 'detail_tv_state.dart';

class DetailTvBloc extends Bloc<DetailTvEvent, DetailTvState> {
  final GetTvDetail _getTvDetail;
  final GetTvRecommendations _getTvRecommendations;

  DetailTvBloc(this._getTvDetail, this._getTvRecommendations)
      : super(DetailTvInitial());

  @override
  Stream<DetailTvState> mapEventToState(
    DetailTvEvent event,
  ) async* {
    if (event is FetchDetailTv) {
      yield DetailTvLoading();
      final resultDetail = await _getTvDetail.execute(event.id);
      yield* resultDetail.fold((failed) async* {
        yield DetailTvError(failed.message);
      }, (dataDetail) async* {
        final resultRecommendation =
            await _getTvRecommendations.execute(event.id);

        yield* resultRecommendation.fold((failed) async* {
          yield DetailTvError(failed.message);
        }, (dataRecommendation) async* {
          yield DetailTvLoaded(
              tv: dataDetail, recommendation: dataRecommendation);
        });
      });
    }
  }
}
