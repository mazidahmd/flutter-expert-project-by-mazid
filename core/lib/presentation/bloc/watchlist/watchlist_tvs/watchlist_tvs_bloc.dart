import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/usecases/get_watchlist_tvs.dart';
import 'package:equatable/equatable.dart';

part 'watchlist_tvs_event.dart';
part 'watchlist_tvs_state.dart';

class WatchlistTvsBloc extends Bloc<WatchlistTvsEvent, WatchlistTvsState> {
  final GetWatchlistTvs _getWatchlistTvs;
  WatchlistTvsBloc(this._getWatchlistTvs) : super(WatchlistTvsInitial());

  @override
  Stream<WatchlistTvsState> mapEventToState(
    WatchlistTvsEvent event,
  ) async* {
    if (event is FetchWatchlistTvs) {
      yield WatchlistTvsLoading();
      var result = await _getWatchlistTvs.execute();

      yield* result.fold((failed) async* {
        yield WatchlistTvsError(failed.message);
      }, (data) async* {
        yield WatchlistTvsLoaded(data);
      });
    }
  }
}
