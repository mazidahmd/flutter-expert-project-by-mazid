import 'package:core/presentation/pages/home_movie_page.dart';
import 'package:core/presentation/widgets/main_drawer.dart';
import 'package:core/utils/routes.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget _currentHome = HomeMoviePage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(
        onCurrentWidget: (widget) {
          setState(() {
            _currentHome = widget;
          });
        },
      ),
      appBar: AppBar(
        title: Text('Ditonton'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, SEARCH_ROUTE);
            },
            icon: const Icon(Icons.search),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: _currentHome,
        ),
      ),
    );
  }
}
