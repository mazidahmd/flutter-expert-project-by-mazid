import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../dummy_data/dummy_objects.dart';
import 'status_watchlist_tv_bloc_test.mocks.dart';

@GenerateMocks([GetWatchListStatusTv, SaveWatchlistTv, RemoveWatchlistTv])
void main() {
  late StatusWatchlistTvBloc statusWatchlistTv;
  late MockGetWatchListStatusTv mockGetWatchListStatus;
  late MockSaveWatchlistTv mockSaveWatchlist;
  late MockRemoveWatchlistTv mockRemoveWatchlist;

  setUp(() {
    mockGetWatchListStatus = MockGetWatchListStatusTv();
    mockSaveWatchlist = MockSaveWatchlistTv();
    mockRemoveWatchlist = MockRemoveWatchlistTv();
    statusWatchlistTv = StatusWatchlistTvBloc(
        mockGetWatchListStatus, mockSaveWatchlist, mockRemoveWatchlist);
  });

  tearDown(() {
    statusWatchlistTv.close();
  });

  test('initial statusWatchlistTv state should be initial', () {
    expect(statusWatchlistTv.state, StatusWatchlistTvInitial());
  });

  blocTest<StatusWatchlistTvBloc, StatusWatchlistTvState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetWatchListStatus.execute(1)).thenAnswer((_) async => false);
      return statusWatchlistTv;
    },
    act: (bloc) => bloc.add(FetchStatusWatchlistTv(1)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistTvLoading(),
      StatusWatchlistTvLoaded(isWishlisted: false),
    ],
    verify: (bloc) {
      verify(mockGetWatchListStatus.execute(1));
    },
  );

  blocTest<StatusWatchlistTvBloc, StatusWatchlistTvState>(
    'Should emit [Loading, HasData] when add to watchlist is gotten successfully',
    build: () {
      when(mockSaveWatchlist.execute(testTvDetail))
          .thenAnswer((_) async => Right('Saved to watchlist'));
      return statusWatchlistTv;
    },
    act: (bloc) => bloc.add(AddToWatchlistTv(testTvDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistTvLoading(),
      StatusWatchlistTvLoaded(
          isWishlisted: true, message: 'Saved to watchlist'),
    ],
    verify: (bloc) {
      verify(mockSaveWatchlist.execute(testTvDetail));
    },
  );

  blocTest<StatusWatchlistTvBloc, StatusWatchlistTvState>(
    'Should emit [Loading, Error] when add to watchlist is error',
    build: () {
      when(mockSaveWatchlist.execute(testTvDetail))
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return statusWatchlistTv;
    },
    act: (bloc) => bloc.add(AddToWatchlistTv(testTvDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistTvLoading(),
      StatusWatchlistTvError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockSaveWatchlist.execute(testTvDetail));
    },
  );

  blocTest<StatusWatchlistTvBloc, StatusWatchlistTvState>(
    'Should emit [Loading, HasData] when remove from watchlist is gotten successfully',
    build: () {
      when(mockRemoveWatchlist.execute(testTvDetail))
          .thenAnswer((_) async => Right('Removed from watchlist'));
      return statusWatchlistTv;
    },
    act: (bloc) => bloc.add(RemoveFromWatchlistTv(testTvDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistTvLoading(),
      StatusWatchlistTvLoaded(
          isWishlisted: false, message: 'Removed from watchlist'),
    ],
    verify: (bloc) {
      verify(mockRemoveWatchlist.execute(testTvDetail));
    },
  );

  blocTest<StatusWatchlistTvBloc, StatusWatchlistTvState>(
    'Should emit [Loading, Error] when remove from watchlist is error',
    build: () {
      when(mockRemoveWatchlist.execute(testTvDetail))
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return statusWatchlistTv;
    },
    act: (bloc) => bloc.add(RemoveFromWatchlistTv(testTvDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistTvLoading(),
      StatusWatchlistTvError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockRemoveWatchlist.execute(testTvDetail));
    },
  );
}
