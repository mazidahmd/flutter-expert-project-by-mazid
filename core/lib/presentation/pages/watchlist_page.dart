import 'package:core/presentation/pages/watchlist_movies_page.dart';
import 'package:core/presentation/pages/watchlist_tvs_page.dart';
import 'package:flutter/material.dart';

import '../../core.dart';

class WatchlistPage extends StatefulWidget {
  @override
  _WatchlistPageState createState() => _WatchlistPageState();
}

class _WatchlistPageState extends State<WatchlistPage> with RouteAware {
  final List<String> _types = ["Movies", "TV Shows"];
  String _currentType = "Movies";

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Watchlist'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  CustomChip(
                      label: _types[0],
                      isSelected: _currentType == _types[0],
                      onPressed: () {
                        setState(() {
                          _currentType = _types[0];
                        });
                      }),
                  SizedBox(width: 10),
                  CustomChip(
                      label: _types[1],
                      isSelected: _currentType == _types[1],
                      onPressed: () {
                        setState(() {
                          _currentType = _types[1];
                        });
                      }),
                ],
              ),
            ),
            SizedBox(height: 10),
            Expanded(
                child: _currentType == _types[0]
                    ? WatchlistMoviesPage()
                    : WatchlistTvsPage()),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}

class CustomChip extends StatelessWidget {
  const CustomChip(
      {Key? key,
      required this.onPressed,
      required this.isSelected,
      required this.label})
      : super(key: key);

  final String label;
  final VoidCallback onPressed;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return ActionChip(
      onPressed: onPressed,
      backgroundColor: isSelected
          ? Theme.of(context).colorScheme.onBackground
          : Theme.of(context).colorScheme.onBackground.withOpacity(0.1),
      label: Text(
        label,
        style: TextStyle(
            color: isSelected
                ? Theme.of(context).colorScheme.background
                : Theme.of(context).colorScheme.onBackground),
      ),
    );
  }
}
