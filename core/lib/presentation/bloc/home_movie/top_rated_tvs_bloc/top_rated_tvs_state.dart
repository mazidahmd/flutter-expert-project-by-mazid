part of 'top_rated_tvs_bloc.dart';

abstract class TopRatedTvsState extends Equatable {
  @override
  List<Object> get props => [];
}

class TopRatedTvsEmpty extends TopRatedTvsState {}

class TopRatedTvsLoading extends TopRatedTvsState {}

class TopRatedTvsLoaded extends TopRatedTvsState {
  final List<Tv> tvs;

  TopRatedTvsLoaded(this.tvs);
}

class TopRatedTvsError extends TopRatedTvsState {
  final String message;

  TopRatedTvsError(this.message);
}
