import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:core/domain/usecases/get_now_playing_tvs.dart';
import 'package:equatable/equatable.dart';

part 'now_playing_tvs_event.dart';
part 'now_playing_tvs_state.dart';

class NowPlayingTvsBloc extends Bloc<NowPlayingTvsEvent, NowPlayingTvsState> {
  final GetNowPlayingTvs _getNowPlayingTvs;
  NowPlayingTvsBloc(this._getNowPlayingTvs) : super(NowPlayingTvsEmpty());

  @override
  Stream<NowPlayingTvsState> mapEventToState(
    NowPlayingTvsEvent event,
  ) async* {
    if (event is FetchNowPlayingTvsEvent) {
      yield NowPlayingTvsLoading();
      final result = await _getNowPlayingTvs.execute();
      yield* result.fold((failed) async* {
        yield NowPlayingTvsError(failed.message);
      }, (data) async* {
        yield NowPlayingTvsLoaded(data);
      });
    }
  }
}
