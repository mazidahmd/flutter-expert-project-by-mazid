import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv_detail.dart';
import 'package:core/domain/usecases/get_watchlist_status_tv.dart';
import 'package:core/domain/usecases/remove_watchlist_tv.dart';
import 'package:core/domain/usecases/save_watchlist_tv.dart';
import 'package:equatable/equatable.dart';

part 'status_watchlist_tv_event.dart';
part 'status_watchlist_tv_state.dart';

class StatusWatchlistTvBloc
    extends Bloc<StatusWatchlistTvEvent, StatusWatchlistTvState> {
  final GetWatchListStatusTv _getWatchListStatus;
  final SaveWatchlistTv _addToWatchlist;
  final RemoveWatchlistTv _removeFromWatchlist;

  StatusWatchlistTvBloc(
    this._getWatchListStatus,
    this._addToWatchlist,
    this._removeFromWatchlist,
  ) : super(StatusWatchlistTvInitial());

  @override
  Stream<StatusWatchlistTvState> mapEventToState(
    StatusWatchlistTvEvent event,
  ) async* {
    if (event is FetchStatusWatchlistTv) {
      yield StatusWatchlistTvLoading();
      final result = await _getWatchListStatus.execute(event.id);
      yield StatusWatchlistTvLoaded(isWishlisted: result);
    }

    if (event is AddToWatchlistTv) {
      yield StatusWatchlistTvLoading();
      final result = await _addToWatchlist.execute(event.tv);
      yield* result.fold((failed) async* {
        yield StatusWatchlistTvError(failed.message);
      }, (data) async* {
        yield StatusWatchlistTvLoaded(isWishlisted: true, message: data);
      });
    }

    if (event is RemoveFromWatchlistTv) {
      yield StatusWatchlistTvLoading();
      final result = await _removeFromWatchlist.execute(event.tv);
      yield* result.fold((failed) async* {
        yield StatusWatchlistTvError(failed.message);
      }, (data) async* {
        yield StatusWatchlistTvLoaded(isWishlisted: false, message: data);
      });
    }
  }
}
