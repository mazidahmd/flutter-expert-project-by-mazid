import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/domain/usecases/get_top_rated_movies.dart';
import 'package:equatable/equatable.dart';

part 'top_rated_movies_event.dart';
part 'top_rated_movies_state.dart';

class TopRatedMoviesBloc
    extends Bloc<TopRatedMoviesEvent, TopRatedMoviesState> {
  final GetTopRatedMovies _getTopRatedMovies;
  TopRatedMoviesBloc(this._getTopRatedMovies) : super(TopRatedMoviesEmpty());

  @override
  Stream<TopRatedMoviesState> mapEventToState(
    TopRatedMoviesEvent event,
  ) async* {
    if (event is FetchTopRatedMoviesEvent) {
      yield TopRatedMoviesLoading();
      final result = await _getTopRatedMovies.execute();
      yield* result.fold((failed) async* {
        yield TopRatedMoviesError(failed.message);
      }, (data) async* {
        yield TopRatedMoviesLoaded(data);
      });
    }
  }
}
