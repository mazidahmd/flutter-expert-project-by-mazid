part of 'status_watchlist_tv_bloc.dart';

abstract class StatusWatchlistTvEvent extends Equatable {
  const StatusWatchlistTvEvent();

  @override
  List<Object> get props => [];
}

class FetchStatusWatchlistTv extends StatusWatchlistTvEvent {
  final int id;

  FetchStatusWatchlistTv(this.id);
}

class AddToWatchlistTv extends StatusWatchlistTvEvent {
  final TvDetail tv;

  AddToWatchlistTv(this.tv);
}

class RemoveFromWatchlistTv extends StatusWatchlistTvEvent {
  final TvDetail tv;

  RemoveFromWatchlistTv(this.tv);
}
