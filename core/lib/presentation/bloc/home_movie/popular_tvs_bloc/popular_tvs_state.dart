part of 'popular_tvs_bloc.dart';

abstract class PopularTvsState extends Equatable {
  @override
  List<Object> get props => [];
}

class PopularTvsEmpty extends PopularTvsState {}

class PopularTvsLoading extends PopularTvsState {}

class PopularTvsLoaded extends PopularTvsState {
  final List<Tv> tvs;

  PopularTvsLoaded(this.tvs);
}

class PopularTvsError extends PopularTvsState {
  final String message;

  PopularTvsError(this.message);
}
