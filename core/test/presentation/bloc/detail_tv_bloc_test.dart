import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../dummy_data/dummy_objects.dart';
import 'detail_tv_bloc_test.mocks.dart';

@GenerateMocks([GetTvDetail, GetTvRecommendations])
void main() {
  late DetailTvBloc detailTvBloc;
  late MockGetTvDetail mockGetTvDetail;
  late MockGetTvRecommendations mockGetTvRecommendations;

  setUp(() {
    mockGetTvDetail = MockGetTvDetail();
    mockGetTvRecommendations = MockGetTvRecommendations();
    detailTvBloc = DetailTvBloc(mockGetTvDetail, mockGetTvRecommendations);
  });

  tearDown(() {
    detailTvBloc.close();
  });

  test('initial DetailTvBloc state should be initial', () {
    expect(detailTvBloc.state, DetailTvInitial());
  });

  blocTest<DetailTvBloc, DetailTvState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetTvDetail.execute(1))
          .thenAnswer((_) async => Right(testTvDetail));
      when(mockGetTvRecommendations.execute(1))
          .thenAnswer((_) async => Right(<Tv>[]));
      return detailTvBloc;
    },
    act: (bloc) => bloc.add(FetchDetailTv(1)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      DetailTvLoading(),
      DetailTvLoaded(tv: testTvDetail, recommendation: <Tv>[]),
    ],
    verify: (bloc) {
      verify(mockGetTvDetail.execute(1));
      verify(mockGetTvRecommendations.execute(1));
    },
  );
}
