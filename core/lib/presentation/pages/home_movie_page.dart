import 'package:core/core.dart';
import 'package:core/presentation/bloc/home_movie/now_playing_movies_bloc/now_playing_movies_bloc.dart';
import 'package:core/presentation/bloc/home_movie/popular_movies_bloc/popular_movies_bloc.dart';
import 'package:core/presentation/bloc/home_movie/top_rated_movies_bloc/top_rated_movies_bloc.dart';
import 'package:core/presentation/widgets/movie_list_container.dart';
import 'package:core/utils/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class HomeMoviePage extends StatefulWidget {
  @override
  _HomeMoviePageState createState() => _HomeMoviePageState();
}

class _HomeMoviePageState extends State<HomeMoviePage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<NowPlayingMoviesBloc>().add(FetchNowPlayingMoviesEvent());
      context.read<TopRatedMoviesBloc>().add(FetchTopRatedMoviesEvent());
      context.read<PopularMoviesBloc>().add(FetchPopularMoviesEvent());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            'Movies',
            style: kHeading5,
          ),
        ),
        SizedBox(height: 10),
        Text(
          'Now Playing',
          style: kHeading6,
        ),
        BlocBuilder<NowPlayingMoviesBloc, NowPlayingMoviesState>(
            builder: (context, state) {
          if (state is NowPlayingMoviesLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is NowPlayingMoviesLoaded) {
            return MovieListContainer(state.movies);
          } else {
            return Text('Failed');
          }
        }),
        _buildSubHeading(
          title: 'Popular',
          onTap: () => Navigator.pushNamed(context, POPULAR_MOVIES_ROUTE),
        ),
        BlocBuilder<PopularMoviesBloc, PopularMoviesState>(
            builder: (context, state) {
          if (state is PopularMoviesLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is PopularMoviesLoaded) {
            return MovieListContainer(state.movies);
          } else {
            return Text('Failed');
          }
        }),
        _buildSubHeading(
          title: 'Top Rated',
          onTap: () => Navigator.pushNamed(context, TOP_RATED_ROUTE),
        ),
        BlocBuilder<TopRatedMoviesBloc, TopRatedMoviesState>(
            builder: (context, state) {
          if (state is TopRatedMoviesLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is TopRatedMoviesLoaded) {
            return MovieListContainer(state.movies);
          } else {
            return const Text('Failed');
          }
        }),
      ],
    );
  }

  Row _buildSubHeading({required String title, required Function() onTap}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: kHeading6,
        ),
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [Text('See More'), Icon(Icons.arrow_forward_ios)],
            ),
          ),
        ),
      ],
    );
  }
}
