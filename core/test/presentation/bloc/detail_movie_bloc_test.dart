import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../dummy_data/dummy_objects.dart';
import 'detail_movie_bloc_test.mocks.dart';

@GenerateMocks([GetMovieDetail, GetMovieRecommendations])
void main() {
  late DetailMovieBloc detailMovieBloc;
  late MockGetMovieDetail mockGetMovieDetail;
  late MockGetMovieRecommendations mockGetMovieRecommendations;

  setUp(() {
    mockGetMovieDetail = MockGetMovieDetail();
    mockGetMovieRecommendations = MockGetMovieRecommendations();
    detailMovieBloc =
        DetailMovieBloc(mockGetMovieDetail, mockGetMovieRecommendations);
  });

  tearDown(() {
    detailMovieBloc.close();
  });

  test('initial DetailMovieBloc state should be initial', () {
    expect(detailMovieBloc.state, DetailMovieInitial());
  });

  blocTest<DetailMovieBloc, DetailMovieState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetMovieDetail.execute(1))
          .thenAnswer((_) async => Right(testMovieDetail));
      when(mockGetMovieRecommendations.execute(1))
          .thenAnswer((_) async => Right(<Movie>[]));
      return detailMovieBloc;
    },
    act: (bloc) => bloc.add(FetchDetailMovie(1)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      DetailMovieLoading(),
      DetailMovieLoaded(movie: testMovieDetail, recommendation: <Movie>[]),
    ],
    verify: (bloc) {
      verify(mockGetMovieDetail.execute(1));
      verify(mockGetMovieRecommendations.execute(1));
    },
  );
}
