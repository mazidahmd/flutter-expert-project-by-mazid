part of 'detail_tv_bloc.dart';

abstract class DetailTvEvent extends Equatable {
  const DetailTvEvent();

  @override
  List<Object> get props => [];
}

class FetchDetailTv extends DetailTvEvent {
  final int id;

  FetchDetailTv(this.id);
}
