import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../dummy_data/dummy_objects.dart';
import 'status_watchlist_movie_bloc_test.mocks.dart';

@GenerateMocks([GetWatchListStatus, SaveWatchlist, RemoveWatchlist])
void main() {
  late StatusWatchlistMovieBloc statusWatchlistMovie;
  late MockGetWatchListStatus mockGetWatchListStatus;
  late MockSaveWatchlist mockSaveWatchlist;
  late MockRemoveWatchlist mockRemoveWatchlist;

  setUp(() {
    mockGetWatchListStatus = MockGetWatchListStatus();
    mockSaveWatchlist = MockSaveWatchlist();
    mockRemoveWatchlist = MockRemoveWatchlist();
    statusWatchlistMovie = StatusWatchlistMovieBloc(
        mockGetWatchListStatus, mockSaveWatchlist, mockRemoveWatchlist);
  });

  tearDown(() {
    statusWatchlistMovie.close();
  });

  test('initial statusWatchlistMovie state should be initial', () {
    expect(statusWatchlistMovie.state, StatusWatchlistMovieInitial());
  });

  blocTest<StatusWatchlistMovieBloc, StatusWatchlistMovieState>(
    'Should emit [Loading, HasData] when data is gotten successfully',
    build: () {
      when(mockGetWatchListStatus.execute(1)).thenAnswer((_) async => false);
      return statusWatchlistMovie;
    },
    act: (bloc) => bloc.add(FetchStatusWatchlistMovie(1)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistMovieLoading(),
      StatusWatchlistMovieLoaded(isWishlisted: false),
    ],
    verify: (bloc) {
      verify(mockGetWatchListStatus.execute(1));
    },
  );

  blocTest<StatusWatchlistMovieBloc, StatusWatchlistMovieState>(
    'Should emit [Loading, HasData] when add to watchlist is gotten successfully',
    build: () {
      when(mockSaveWatchlist.execute(testMovieDetail))
          .thenAnswer((_) async => Right('Saved to watchlist'));
      return statusWatchlistMovie;
    },
    act: (bloc) => bloc.add(AddToWatchlist(testMovieDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistMovieLoading(),
      StatusWatchlistMovieLoaded(
          isWishlisted: true, message: 'Saved to watchlist'),
    ],
    verify: (bloc) {
      verify(mockSaveWatchlist.execute(testMovieDetail));
    },
  );

  blocTest<StatusWatchlistMovieBloc, StatusWatchlistMovieState>(
    'Should emit [Loading, Error] when add to watchlist is error',
    build: () {
      when(mockSaveWatchlist.execute(testMovieDetail))
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return statusWatchlistMovie;
    },
    act: (bloc) => bloc.add(AddToWatchlist(testMovieDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistMovieLoading(),
      StatusWatchlistMovieError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockSaveWatchlist.execute(testMovieDetail));
    },
  );

  blocTest<StatusWatchlistMovieBloc, StatusWatchlistMovieState>(
    'Should emit [Loading, HasData] when remove from watchlist is gotten successfully',
    build: () {
      when(mockRemoveWatchlist.execute(testMovieDetail))
          .thenAnswer((_) async => Right('Removed from watchlist'));
      return statusWatchlistMovie;
    },
    act: (bloc) => bloc.add(RemoveFromWatchlist(testMovieDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistMovieLoading(),
      StatusWatchlistMovieLoaded(
          isWishlisted: false, message: 'Removed from watchlist'),
    ],
    verify: (bloc) {
      verify(mockRemoveWatchlist.execute(testMovieDetail));
    },
  );

  blocTest<StatusWatchlistMovieBloc, StatusWatchlistMovieState>(
    'Should emit [Loading, Error] when remove from watchlist is error',
    build: () {
      when(mockRemoveWatchlist.execute(testMovieDetail))
          .thenAnswer((_) async => Left(ServerFailure('Something wrong')));
      return statusWatchlistMovie;
    },
    act: (bloc) => bloc.add(RemoveFromWatchlist(testMovieDetail)),
    wait: const Duration(milliseconds: 500),
    expect: () => [
      StatusWatchlistMovieLoading(),
      StatusWatchlistMovieError('Something wrong'),
    ],
    verify: (bloc) {
      verify(mockRemoveWatchlist.execute(testMovieDetail));
    },
  );
}
