import 'dart:convert';

import 'package:core/data/models/tv_detail_model.dart';
import 'package:core/data/models/tv_response.dart';
import 'package:core/utils/exception.dart';
import 'package:core/data/models/tv_model.dart';
import 'package:core/utils/network.dart';
import 'package:http/http.dart' as http;

abstract class TvRemoteDataSource {
  Future<List<TvModel>> getNowPlayingTv();
  Future<List<TvModel>> getPopularTv();
  Future<List<TvModel>> getTopRatedTv();
  Future<TvDetailModel> getTvDetail(int id);
  Future<List<TvModel>> getTvRecommendations(int id);
  Future<List<TvModel>> searchTv(String query);
}

class TvRemoteDataSourceImpl implements TvRemoteDataSource {
  static const API_KEY = 'api_key=18e02a0dbb7b8e305e331548c5fa9b78';
  static const BASE_URL = 'https://api.themoviedb.org/3';

  final http.Client client;
  final NetworkUtils network;

  TvRemoteDataSourceImpl({required this.client, required this.network});

  @override
  Future<List<TvModel>> getNowPlayingTv() async {
    final response =
        await network.requestGet(client, '$BASE_URL/tv/airing_today?$API_KEY');

    if (response.statusCode == 200) {
      return TvResponse.fromJson(json.decode(response.body)).tvList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<TvDetailModel> getTvDetail(int id) async {
    final response =
        await network.requestGet(client, '$BASE_URL/tv/$id?$API_KEY');

    if (response.statusCode == 200) {
      return TvDetailModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<TvModel>> getTvRecommendations(int id) async {
    final response = await network.requestGet(
        client, '$BASE_URL/tv/$id/recommendations?$API_KEY');

    if (response.statusCode == 200) {
      return TvResponse.fromJson(json.decode(response.body)).tvList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<TvModel>> getPopularTv() async {
    final response =
        await network.requestGet(client, '$BASE_URL/tv/popular?$API_KEY');

    if (response.statusCode == 200) {
      return TvResponse.fromJson(json.decode(response.body)).tvList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<TvModel>> getTopRatedTv() async {
    final response =
        await network.requestGet(client, '$BASE_URL/tv/top_rated?$API_KEY');

    if (response.statusCode == 200) {
      return TvResponse.fromJson(json.decode(response.body)).tvList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<TvModel>> searchTv(String query) async {
    final response = await network.requestGet(
        client, '$BASE_URL/search/tv?$API_KEY&query=$query');

    if (response.statusCode == 200) {
      return TvResponse.fromJson(json.decode(response.body)).tvList;
    } else {
      throw ServerException();
    }
  }
}
