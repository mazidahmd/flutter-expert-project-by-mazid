part of 'detail_movie_bloc.dart';

abstract class DetailMovieState extends Equatable {
  const DetailMovieState();

  @override
  List<Object> get props => [];
}

class DetailMovieInitial extends DetailMovieState {}

class DetailMovieEmpty extends DetailMovieState {}

class DetailMovieLoading extends DetailMovieState {}

class DetailMovieLoaded extends DetailMovieState {
  final MovieDetail movie;
  final List<Movie> recommendation;

  DetailMovieLoaded({required this.movie, required this.recommendation});
}

class DetailMovieError extends DetailMovieState {
  final String message;

  DetailMovieError(this.message);
}
