import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/presentation/widgets/movie_list_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'popular_movies_page_test.dart';
import 'top_rated_movies_page_test.dart';

class MockNowPlayingMoviesBloc
    extends MockBloc<NowPlayingMoviesEvent, NowPlayingMoviesState>
    implements NowPlayingMoviesBloc {}

class MockNowPlayingMoviesEvent extends Fake implements NowPlayingMoviesEvent {}

class MockNowPlayingMoviesState extends Fake implements NowPlayingMoviesState {}

void main() {
  late MockNowPlayingMoviesBloc mockNowPlayingMoviesBloc;
  late MockPopularMoviesBloc mockPopularMoviesBloc;
  late MockTopRatedMoviesBloc mockTopRatedMoviesBloc;

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    mockNowPlayingMoviesBloc = MockNowPlayingMoviesBloc();
    mockPopularMoviesBloc = MockPopularMoviesBloc();
    mockTopRatedMoviesBloc = MockTopRatedMoviesBloc();
  });

  setUpAll(() {
    registerFallbackValue(MockNowPlayingMoviesEvent());
    registerFallbackValue(MockNowPlayingMoviesState());
    registerFallbackValue(MockPopularMoviesEvent());
    registerFallbackValue(MockPopularMoviesState());
    registerFallbackValue(MockTopRatedMoviesEvent());
    registerFallbackValue(MockTopRatedMoviesState());
  });

  tearDown(() {
    mockNowPlayingMoviesBloc.close();
    mockTopRatedMoviesBloc.close();
    mockPopularMoviesBloc.close();
  });

  Widget _makeTestableWidget(Widget body) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NowPlayingMoviesBloc>(
            create: (context) => mockNowPlayingMoviesBloc),
        BlocProvider<PopularMoviesBloc>(
            create: (context) => mockPopularMoviesBloc),
        BlocProvider<TopRatedMoviesBloc>(
            create: (context) => mockTopRatedMoviesBloc)
      ],
      child: MaterialApp(
        home: body,
      ),
    );
  }

  testWidgets('Should build home page', (WidgetTester tester) async {
    when(() => mockNowPlayingMoviesBloc.state)
        .thenAnswer((_) => NowPlayingMoviesLoaded(<Movie>[
              Movie(
                  adult: true,
                  backdropPath: "backdropPath",
                  genreIds: [1, 2],
                  id: 1,
                  originalTitle: "originalTitle",
                  overview: "overview",
                  popularity: 5.1,
                  posterPath: "posterPath",
                  releaseDate: "releaseDate",
                  title: "title",
                  video: true,
                  voteAverage: 6.1,
                  voteCount: 78)
            ]));

    when(() => mockTopRatedMoviesBloc.state)
        .thenAnswer((_) => TopRatedMoviesLoaded(<Movie>[
              Movie(
                  adult: true,
                  backdropPath: "backdropPath",
                  genreIds: [1, 2],
                  id: 1,
                  originalTitle: "originalTitle",
                  overview: "overview",
                  popularity: 5.1,
                  posterPath: "posterPath",
                  releaseDate: "releaseDate",
                  title: "title",
                  video: true,
                  voteAverage: 6.1,
                  voteCount: 78)
            ]));

    when(() => mockPopularMoviesBloc.state)
        .thenAnswer((_) => PopularMoviesLoaded(<Movie>[
              Movie(
                  adult: true,
                  backdropPath: "backdropPath",
                  genreIds: [1, 2],
                  id: 1,
                  originalTitle: "originalTitle",
                  overview: "overview",
                  popularity: 5.1,
                  posterPath: "posterPath",
                  releaseDate: "releaseDate",
                  title: "title",
                  video: true,
                  voteAverage: 6.1,
                  voteCount: 78)
            ]));

    // final mainDrawer = find.byType(MainDrawer);
    final findMovieListContainer = find.byType(MovieListContainer);

    await tester.pumpWidget(_makeTestableWidget(HomePage()));

    expect(findMovieListContainer, findsNWidgets(3));
    // expect(mainDrawer, findsOneWidget);
  });
}
