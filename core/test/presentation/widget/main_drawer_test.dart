import 'package:core/presentation/widgets/main_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  Widget _makeTestableWidget(Widget body) {
    return MaterialApp(
      home: body,
    );
  }

  testWidgets('Should build main drawer', (WidgetTester tester) async {
    final listTiles = find.byType(ListTile);
    final findDrawer = find.byType(Drawer);

    await tester.pumpWidget(
        _makeTestableWidget(MainDrawer(onCurrentWidget: (currentWidget) {})));

    expect(findDrawer, findsOneWidget);
    expect(listTiles, findsNWidgets(4));
  });
}
