part of 'watchlist_tvs_bloc.dart';

abstract class WatchlistTvsEvent extends Equatable {
  const WatchlistTvsEvent();

  @override
  List<Object> get props => [];
}

class FetchWatchlistTvs extends WatchlistTvsEvent {}
