import 'package:bloc_test/bloc_test.dart';
import 'package:core/core.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/presentation/pages/movie_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../dummy_data/dummy_objects.dart';

class MockDetailMovieBloc extends MockBloc<DetailMovieEvent, DetailMovieState>
    implements DetailMovieBloc {}

class MockStatusWatchlistMovieBloc
    extends MockBloc<StatusWatchlistMovieEvent, StatusWatchlistMovieState>
    implements StatusWatchlistMovieBloc {}

class MockDetailMovieEvent extends Fake implements DetailMovieEvent {}

class MockDetailMovieState extends Fake implements DetailMovieState {}

class MockStatusWatchlistMovieEvent extends Fake
    implements StatusWatchlistMovieEvent {}

class MockStatusWatchlistMovieState extends Fake
    implements StatusWatchlistMovieState {}

void main() {
  late MockDetailMovieBloc mockDetailMovieBloc;
  late MockStatusWatchlistMovieBloc mockStatusWatchlistMovieBloc;

  setUpAll(() {
    registerFallbackValue(MockDetailMovieState());
    registerFallbackValue(MockDetailMovieEvent());
    registerFallbackValue(MockStatusWatchlistMovieState());
    registerFallbackValue(MockStatusWatchlistMovieEvent());
  });

  setUp(() {
    mockDetailMovieBloc = MockDetailMovieBloc();
    mockStatusWatchlistMovieBloc = MockStatusWatchlistMovieBloc();
  });

  Widget _makeTestableWidget(Widget body) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<DetailMovieBloc>(
              create: (context) => mockDetailMovieBloc),
          BlocProvider<StatusWatchlistMovieBloc>(
              create: (context) => mockStatusWatchlistMovieBloc)
        ],
        child: MaterialApp(
          home: body,
        ));
  }

  tearDown(() {
    mockDetailMovieBloc.close();
    mockStatusWatchlistMovieBloc.close();
  });

  testWidgets(
      'Watchlist button should display add icon when movie not added to watchlist',
      (WidgetTester tester) async {
    when(() => mockDetailMovieBloc.add(FetchDetailMovie(1)))
        .thenAnswer((_) async => {});
    when(() => mockDetailMovieBloc.state).thenAnswer((_) =>
        DetailMovieLoaded(movie: testMovieDetail, recommendation: <Movie>[]));
    when(() => mockStatusWatchlistMovieBloc.add(FetchStatusWatchlistMovie(1)))
        .thenAnswer((_) async => {});
    when(() => mockStatusWatchlistMovieBloc.state)
        .thenAnswer((_) => StatusWatchlistMovieLoaded(isWishlisted: false));

    final watchlistButtonIcon = find.byIcon(Icons.add);
    await tester.pumpWidget(_makeTestableWidget(MovieDetailPage(id: 1)));

    expect(watchlistButtonIcon, findsOneWidget);
  });

  testWidgets(
      'Watchlist button should display check icon when movie is added to wathclist',
      (WidgetTester tester) async {
    when(() => mockDetailMovieBloc.add(FetchDetailMovie(1)))
        .thenAnswer((_) async => {});
    when(() => mockDetailMovieBloc.state).thenAnswer((_) =>
        DetailMovieLoaded(movie: testMovieDetail, recommendation: <Movie>[]));
    when(() => mockStatusWatchlistMovieBloc.add(FetchStatusWatchlistMovie(1)))
        .thenAnswer((_) async => {});
    when(() => mockStatusWatchlistMovieBloc.state)
        .thenAnswer((_) => StatusWatchlistMovieLoaded(isWishlisted: true));

    final watchlistButtonIcon = find.byIcon(Icons.check);

    await tester.pumpWidget(_makeTestableWidget(MovieDetailPage(id: 1)));

    expect(watchlistButtonIcon, findsOneWidget);
  });

  // testWidgets(
  //     'Watchlist button should display Snackbar when added to watchlist',
  //     (WidgetTester tester) async {
  //   when(() => mockDetailMovieBloc.add(FetchDetailMovie(1)))
  //       .thenAnswer((_) async => {});
  //   when(() => mockDetailMovieBloc.state).thenAnswer((_) =>
  //       DetailMovieLoaded(movie: testMovieDetail, recommendation: <Movie>[]));
  //   when(() => mockStatusWatchlistMovieBloc.add(FetchStatusWatchlistMovie(1)))
  //       .thenAnswer((_) async => {});
  //   when(() => mockStatusWatchlistMovieBloc.state)
  //       .thenAnswer((_) => StatusWatchlistMovieLoaded(isWishlisted: false));

  //   final watchlistButton = find.byType(ElevatedButton);

  //   await tester.pumpWidget(_makeTestableWidget(MovieDetailPage(id: 1)));

  //   expect(find.byIcon(Icons.add), findsOneWidget);

  //   await tester.pump();
  //   when(() =>
  //           mockStatusWatchlistMovieBloc.add(AddToWatchlist(testMovieDetail)))
  //       .thenAnswer((_) async => {});
  //   when(() => mockStatusWatchlistMovieBloc.state).thenAnswer((_) =>
  //       StatusWatchlistMovieLoaded(
  //           isWishlisted: true, message: 'Added to Watchlist'));

  //   await tester.tap(watchlistButton);
  //   await tester.pump();

  //   expect(find.byType(SnackBar), findsOneWidget);
  //   expect(find.text('Added to Watchlist'), findsOneWidget);
  // });

  // testWidgets(
  //     'Watchlist button should display AlertDialog when add to watchlist failed',
  //     (WidgetTester tester) async {
  //   when(mockNotifier.movieState).thenReturn(RequestState.Loaded);
  //   when(mockNotifier.movie).thenReturn(testMovieDetail);
  //   when(mockNotifier.recommendationState).thenReturn(RequestState.Loaded);
  //   when(mockNotifier.movieRecommendations).thenReturn(<Movie>[]);
  //   when(mockNotifier.isAddedToWatchlist).thenReturn(false);
  //   when(mockNotifier.watchlistMessage).thenReturn('Failed');

  //   final watchlistButton = find.byType(ElevatedButton);

  //   await tester.pumpWidget(_makeTestableWidget(MovieDetailPage(id: 1)));

  //   expect(find.byIcon(Icons.add), findsOneWidget);

  //   await tester.tap(watchlistButton);
  //   await tester.pump();

  //   expect(find.byType(AlertDialog), findsOneWidget);
  //   expect(find.text('Failed'), findsOneWidget);
  // });
}
