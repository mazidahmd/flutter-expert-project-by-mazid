import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/tv.dart';
import 'package:rxdart/rxdart.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:equatable/equatable.dart';
import 'package:search/domain/usecases/search_movies.dart';
import 'package:search/domain/usecases/search_tvs.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchMovies _searchMovies;
  final SearchTvs _searchTvs;

  SearchBloc(this._searchMovies, this._searchTvs) : super(SearchEmpty());

  @override
  Stream<Transition<SearchEvent, SearchState>> transformEvents(
    Stream<SearchEvent> events,
    // ignore: deprecated_member_use
    TransitionFunction<SearchEvent, SearchState> transitionFn,
  ) {
    // ignore: deprecated_member_use
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is OnQueryChanged) {
      final query = event.query;

      yield SearchLoading();
      switch (event.type) {
        case "Movies":
          final result = await _searchMovies.execute(query);

          yield* result.fold(
            (failure) async* {
              yield SearchError(failure.message);
            },
            (data) async* {
              yield SearchHasData(data, const []);
            },
          );
          break;
        default:
          final result = await _searchTvs.execute(query);

          yield* result.fold(
            (failure) async* {
              yield SearchError(failure.message);
            },
            (data) async* {
              yield SearchHasData(const [], data);
            },
          );
      }
    }
  }
}
