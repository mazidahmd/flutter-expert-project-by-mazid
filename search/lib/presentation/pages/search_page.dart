import 'package:core/core.dart';
import 'package:core/presentation/widgets/movie_card_list.dart';
import 'package:core/presentation/widgets/tv_card_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:search/presentation/bloc/search_bloc/search_bloc.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final List<String> _types = ["Movies", "TV Shows"];
  String _currentType = "Movies";
  String _query = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              onChanged: (query) {
                setState(() {
                  _query = query;
                });
                context
                    .read<SearchBloc>()
                    .add(OnQueryChanged(query, _currentType));
              },
              decoration: const InputDecoration(
                hintText: 'Search title',
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(),
              ),
              textInputAction: TextInputAction.search,
            ),
            const SizedBox(height: 16),
            Text(
              'Search Result',
              style: kHeading6,
            ),
            Container(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  CustomChip(
                      label: _types[0],
                      isSelected: _currentType == _types[0],
                      onPressed: () {
                        setState(() {
                          _currentType = _types[0];
                          context
                              .read<SearchBloc>()
                              .add(OnQueryChanged(_query, _currentType));
                        });
                      }),
                  const SizedBox(width: 10),
                  CustomChip(
                      label: _types[1],
                      isSelected: _currentType == _types[1],
                      onPressed: () {
                        setState(() {
                          _currentType = _types[1];
                          context
                              .read<SearchBloc>()
                              .add(OnQueryChanged(_query, _currentType));
                        });
                      }),
                ],
              ),
            ),
            BlocBuilder<SearchBloc, SearchState>(
              builder: (context, state) {
                if (state is SearchLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is SearchHasData) {
                  if (_currentType == _types[0]) {
                    final results = state.result;
                    return Expanded(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemBuilder: (context, index) {
                          final movie = results[index];
                          return MovieCard(movie);
                        },
                        itemCount: results.length,
                      ),
                    );
                  } else {
                    final results = state.resultTv;
                    return Expanded(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemBuilder: (context, index) {
                          final tv = results[index];
                          return TvCard(tv);
                        },
                        itemCount: results.length,
                      ),
                    );
                  }
                } else if (state is SearchError) {
                  return Expanded(
                    child: Center(
                      child: Text(state.message),
                    ),
                  );
                } else {
                  return Expanded(
                    child: Container(),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
