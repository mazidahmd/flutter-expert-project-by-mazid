import 'package:core/core.dart';
import 'package:core/styles/text_styles.dart';
import 'package:core/utils/routes.dart';
import 'package:core/presentation/widgets/tv_list_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class HomeTvPage extends StatefulWidget {
  @override
  _HomeTvPageState createState() => _HomeTvPageState();
}

class _HomeTvPageState extends State<HomeTvPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<NowPlayingTvsBloc>().add(FetchNowPlayingTvsEvent());
      context.read<PopularTvsBloc>().add(FetchPopularTvsEvent());
      context.read<TopRatedTvsBloc>().add(FetchTopRatedTvsEvent());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            'TV Show',
            style: kHeading5,
          ),
        ),
        SizedBox(height: 10),
        Text(
          'Now Playing',
          style: kHeading6,
        ),
        BlocBuilder<NowPlayingTvsBloc, NowPlayingTvsState>(
            builder: (context, state) {
          if (state is NowPlayingMoviesLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is NowPlayingTvsLoaded) {
            return TvListContainer(state.tvs);
          } else {
            return Text('Failed');
          }
        }),
        _buildSubHeading(
          title: 'Popular',
          onTap: () => Navigator.pushNamed(context, POPULAR_TVS_ROUTE),
        ),
        BlocBuilder<PopularTvsBloc, PopularTvsState>(builder: (context, state) {
          if (state is PopularTvsLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is PopularTvsLoaded) {
            return TvListContainer(state.tvs);
          } else {
            return Text('Failed');
          }
        }),
        _buildSubHeading(
          title: 'Top Rated',
          onTap: () => Navigator.pushNamed(context, TOP_RATED_TV_ROUTE),
        ),
        BlocBuilder<TopRatedTvsBloc, TopRatedTvsState>(
            builder: (context, state) {
          if (state is TopRatedTvsLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is TopRatedTvsLoaded) {
            return TvListContainer(state.tvs);
          } else {
            return Text('Failed');
          }
        }),
      ],
    );
  }

  Row _buildSubHeading({required String title, required Function() onTap}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: kHeading6,
        ),
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: const [Text('See More'), Icon(Icons.arrow_forward_ios)],
            ),
          ),
        ),
      ],
    );
  }
}
