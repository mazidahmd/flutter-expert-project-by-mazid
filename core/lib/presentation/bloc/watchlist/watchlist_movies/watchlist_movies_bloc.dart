import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/domain/usecases/get_watchlist_movies.dart';
import 'package:equatable/equatable.dart';

part 'watchlist_movies_event.dart';
part 'watchlist_movies_state.dart';

class WatchlistMoviesBloc
    extends Bloc<WatchlistMoviesEvent, WatchlistMoviesState> {
  final GetWatchlistMovies _getWatchlistMovies;
  WatchlistMoviesBloc(this._getWatchlistMovies)
      : super(WatchlistMoviesInitial());

  @override
  Stream<WatchlistMoviesState> mapEventToState(
    WatchlistMoviesEvent event,
  ) async* {
    if (event is FetchWatchlistMovies) {
      yield WatchlistMoviesLoading();
      var result = await _getWatchlistMovies.execute();

      yield* result.fold((failed) async* {
        yield WatchlistMoviesError(failed.message);
      }, (data) async* {
        yield WatchlistMoviesLoaded(data);
      });
    }
  }
}
