import 'dart:convert';

import 'package:core/data/models/genre_model.dart';
import 'package:core/data/models/season_model.dart';
import 'package:core/domain/entities/tv_detail.dart';
import 'package:equatable/equatable.dart';

TvDetailModel tvDetailModelFromJson(String str) =>
    TvDetailModel.fromJson(json.decode(str));

String tvDetailModelToJson(TvDetailModel data) => json.encode(data.toJson());

class TvDetailModel extends Equatable {
  TvDetailModel({
    this.backdropPath,
    this.createdBy,
    this.episodeRunTime,
    this.firstAirDate,
    this.genres,
    required this.id,
    this.languages,
    this.name,
    this.numberOfEpisodes,
    this.numberOfSeasons,
    this.originalLanguage,
    this.originalName,
    this.overview,
    this.popularity,
    this.posterPath,
    this.seasons,
    this.status,
    this.tagline,
    this.type,
    this.voteAverage,
    this.voteCount,
  });

  final String? backdropPath;
  List<dynamic>? createdBy;
  List<int>? episodeRunTime;
  DateTime? firstAirDate;
  List<GenreModel>? genres;
  int id;
  List<String>? languages;
  String? name;
  int? numberOfEpisodes;
  int? numberOfSeasons;
  String? originalLanguage;
  String? originalName;
  String? overview;
  double? popularity;
  String? posterPath;
  List<SeasonModel>? seasons;
  String? status;
  String? tagline;
  String? type;
  double? voteAverage;
  int? voteCount;

  factory TvDetailModel.fromJson(Map<String, dynamic> json) => TvDetailModel(
        backdropPath: json["backdrop_path"],
        createdBy: json["created_by"] != null
            ? List<dynamic>.from(json["created_by"].map((x) => x))
            : null,
        episodeRunTime: List<int>.from(json["episode_run_time"].map((x) => x)),
        firstAirDate: json["first_air_date"] != null
            ? DateTime.tryParse(json["first_air_date"])
            : null,
        genres: json["genres"] != null
            ? List<GenreModel>.from(
                json["genres"].map((x) => GenreModel.fromJson(x)))
            : null,
        id: json["id"],
        languages: json["languages"] != null
            ? List<String>.from(json["languages"].map((x) => x))
            : null,
        name: json["name"],
        numberOfEpisodes: json["number_of_episodes"],
        numberOfSeasons: json["number_of_seasons"],
        originalLanguage: json["original_language"],
        originalName: json["original_name"],
        overview: json["overview"],
        popularity: json["popularity"].toDouble(),
        posterPath: json["poster_path"],
        seasons: json["seasons"] != null
            ? List<SeasonModel>.from(
                json["seasons"].map((x) => SeasonModel.fromJson(x)))
            : null,
        status: json["status"],
        tagline: json["tagline"],
        type: json["type"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"],
      );

  TvDetail toEntity() {
    return TvDetail(
      id: this.id,
      backdropPath: this.backdropPath,
      createdBy: this.createdBy,
      episodeRunTime: this.episodeRunTime,
      firstAirDate: this.firstAirDate,
      genres: this.genres?.map((genre) => genre.toEntity()).toList(),
      languages: this.languages,
      name: this.name,
      numberOfEpisodes: this.numberOfEpisodes,
      numberOfSeasons: this.numberOfSeasons,
      originalLanguage: this.originalLanguage,
      originalName: this.originalName,
      overview: this.overview,
      popularity: this.popularity,
      posterPath: this.posterPath,
      seasons: this.seasons?.map((season) => season.toEntity()).toList(),
      status: this.status,
      tagline: this.tagline,
      type: this.type,
      voteAverage: this.voteAverage,
      voteCount: this.voteCount,
    );
  }

  Map<String, dynamic> toJson() => {
        "backdrop_path": backdropPath,
        "created_by": createdBy != null
            ? List<dynamic>.from(createdBy!.map((x) => x))
            : null,
        "episode_run_time": episodeRunTime != null
            ? List<dynamic>.from(episodeRunTime!.map((x) => x))
            : null,
        "first_air_date":
            "${firstAirDate?.year.toString().padLeft(4, '0')}-${firstAirDate?.month.toString().padLeft(2, '0')}-${firstAirDate?.day.toString().padLeft(2, '0')}",
        "genres": genres != null
            ? List<dynamic>.from(genres!.map((x) => x.toJson()))
            : null,
        "id": id,
        "languages": languages != null
            ? List<dynamic>.from(languages!.map((x) => x))
            : null,
        "name": name,
        "number_of_episodes": numberOfEpisodes,
        "number_of_seasons": numberOfSeasons,
        "original_language": originalLanguage,
        "original_name": originalName,
        "overview": overview,
        "popularity": popularity,
        "poster_path": posterPath,
        "seasons": seasons != null
            ? List<dynamic>.from(seasons!.map((x) => x.toJson()))
            : null,
        "status": status,
        "tagline": tagline,
        "type": type,
        "vote_average": voteAverage,
        "vote_count": voteCount,
      };

  @override
  List<Object?> get props => [
        backdropPath,
        createdBy,
        episodeRunTime,
        firstAirDate,
        genres,
        id,
        languages,
        name,
        numberOfEpisodes,
        numberOfSeasons,
        originalLanguage,
        originalName,
        overview,
        popularity,
        posterPath,
        status,
        tagline,
        type,
        voteAverage,
        voteCount
      ];
}
