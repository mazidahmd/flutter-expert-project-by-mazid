import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie_detail.dart';
import 'package:core/domain/usecases/get_watchlist_status.dart';
import 'package:core/domain/usecases/remove_watchlist.dart';
import 'package:core/domain/usecases/save_watchlist.dart';
import 'package:equatable/equatable.dart';

part 'status_watchlist_movie_event.dart';
part 'status_watchlist_movie_state.dart';

class StatusWatchlistMovieBloc
    extends Bloc<StatusWatchlistMovieEvent, StatusWatchlistMovieState> {
  final GetWatchListStatus _getWatchListStatus;
  final SaveWatchlist _addToWatchlist;
  final RemoveWatchlist _removeFromWatchlist;

  StatusWatchlistMovieBloc(
    this._getWatchListStatus,
    this._addToWatchlist,
    this._removeFromWatchlist,
  ) : super(StatusWatchlistMovieInitial());

  @override
  Stream<StatusWatchlistMovieState> mapEventToState(
    StatusWatchlistMovieEvent event,
  ) async* {
    if (event is FetchStatusWatchlistMovie) {
      yield StatusWatchlistMovieLoading();
      final result = await _getWatchListStatus.execute(event.id);
      yield StatusWatchlistMovieLoaded(isWishlisted: result);
    }

    if (event is AddToWatchlist) {
      yield StatusWatchlistMovieLoading();
      final result = await _addToWatchlist.execute(event.movie);
      yield* result.fold((failed) async* {
        yield StatusWatchlistMovieError(failed.message);
      }, (data) async* {
        yield StatusWatchlistMovieLoaded(isWishlisted: true, message: data);
      });
    }

    if (event is RemoveFromWatchlist) {
      yield StatusWatchlistMovieLoading();
      final result = await _removeFromWatchlist.execute(event.movie);
      yield* result.fold((failed) async* {
        yield StatusWatchlistMovieError(failed.message);
      }, (data) async* {
        yield StatusWatchlistMovieLoaded(isWishlisted: false, message: data);
      });
    }
  }
}
