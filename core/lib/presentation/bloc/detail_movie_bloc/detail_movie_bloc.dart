import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/domain/entities/movie.dart';
import 'package:core/domain/entities/movie_detail.dart';
import 'package:core/domain/usecases/get_movie_detail.dart';
import 'package:core/domain/usecases/get_movie_recommendations.dart';
import 'package:equatable/equatable.dart';

part 'detail_movie_event.dart';
part 'detail_movie_state.dart';

class DetailMovieBloc extends Bloc<DetailMovieEvent, DetailMovieState> {
  final GetMovieDetail _getMovieDetail;
  final GetMovieRecommendations _getMovieRecommendations;

  DetailMovieBloc(this._getMovieDetail, this._getMovieRecommendations)
      : super(DetailMovieInitial());

  @override
  Stream<DetailMovieState> mapEventToState(
    DetailMovieEvent event,
  ) async* {
    if (event is FetchDetailMovie) {
      yield DetailMovieLoading();
      final resultDetail = await _getMovieDetail.execute(event.id);
      yield* resultDetail.fold((failed) async* {
        yield DetailMovieError(failed.message);
      }, (dataDetail) async* {
        final resultRecommendation =
            await _getMovieRecommendations.execute(event.id);

        yield* resultRecommendation.fold((failed) async* {
          yield DetailMovieError(failed.message);
        }, (dataRecommendation) async* {
          yield DetailMovieLoaded(
              movie: dataDetail, recommendation: dataRecommendation);
        });
      });
    }
  }
}
