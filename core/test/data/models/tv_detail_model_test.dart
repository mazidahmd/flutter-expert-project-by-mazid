import 'dart:convert';

import 'package:core/data/models/genre_model.dart';
import 'package:core/data/models/season_model.dart';
import 'package:core/data/models/tv_detail_model.dart';
import 'package:core/domain/entities/genre.dart';
import 'package:core/domain/entities/season.dart';
import 'package:core/domain/entities/tv_detail.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../json_reader.dart';

void main() {
  final tTvDetailModel = TvDetailModel(
      backdropPath: '/eyG9srihv68ScRdEbJZj66WT4O0.jpg',
      id: 60735,
      originalName: 'The Flash',
      overview:
          "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
      popularity: 764.418,
      posterPath: '/lJA2RCMfsWoskqlQhXPSLFQGXEJ.jpg',
      name: 'The Flash',
      voteAverage: 7.8,
      voteCount: 8687,
      createdBy: ['John Doe'],
      episodeRunTime: [44],
      firstAirDate: DateTime.parse('2014-10-07'),
      genres: [
        GenreModel(id: 18, name: "Drama"),
        GenreModel(id: 10765, name: "Sci-Fi & Fantasy")
      ],
      languages: ["en"],
      numberOfEpisodes: 152,
      numberOfSeasons: 8,
      originalLanguage: "en",
      seasons: [
        SeasonModel(
            id: 79954,
            name: "Specials",
            airDate: DateTime.parse("2016-04-19"),
            episodeCount: 8,
            overview: "",
            posterPath: "/ft8pUr3qX41kOux5eqrwf02yAxZ.jpg",
            seasonNumber: 0)
      ],
      status: "Returning Series",
      tagline: "The fastest man alive.",
      type: "Scripted");

  final tTvDetail = TvDetail(
      backdropPath: '/eyG9srihv68ScRdEbJZj66WT4O0.jpg',
      id: 60735,
      originalName: 'The Flash',
      overview:
          "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
      popularity: 764.418,
      posterPath: '/lJA2RCMfsWoskqlQhXPSLFQGXEJ.jpg',
      name: 'The Flash',
      voteAverage: 7.8,
      voteCount: 8687,
      createdBy: ['John Doe'],
      episodeRunTime: [44],
      firstAirDate: DateTime.parse('2014-10-07'),
      genres: [
        Genre(id: 18, name: "Drama"),
        Genre(id: 10765, name: "Sci-Fi & Fantasy")
      ],
      languages: ["en"],
      numberOfEpisodes: 152,
      numberOfSeasons: 8,
      originalLanguage: "en",
      seasons: [
        Season(
            id: 79954,
            name: "Specials",
            airDate: DateTime.parse("2016-04-19"),
            episodeCount: 8,
            overview: "",
            posterPath: "/ft8pUr3qX41kOux5eqrwf02yAxZ.jpg",
            seasonNumber: 0)
      ],
      status: "Returning Series",
      tagline: "The fastest man alive.",
      type: "Scripted");

  test('should be a subclass of Movie entity', () async {
    final result = tTvDetailModel.toEntity();
    expect(result, tTvDetail);
  });

  group('fromJson', () {
    test('should return a valid model from JSON', () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(readJson('dummy_data/tv_detail.json'));
      // act
      final result = TvDetailModel.fromJson(jsonMap);
      // assert
      expect(result, tTvDetailModel);
    });
  });

  group('toJson', () {
    test('should return a JSON map containing proper data', () async {
      // arrange

      // act
      final result = tTvDetailModel.toJson();
      // assert
      final expectedJsonMap = {
        "backdrop_path": "/eyG9srihv68ScRdEbJZj66WT4O0.jpg",
        "created_by": ["John Doe"],
        "episode_run_time": [44],
        "first_air_date": "2014-10-07",
        "genres": [
          {"id": 18, "name": "Drama"},
          {"id": 10765, "name": "Sci-Fi & Fantasy"}
        ],
        "id": 60735,
        "languages": ["en"],
        "name": "The Flash",
        "number_of_episodes": 152,
        "number_of_seasons": 8,
        "original_language": "en",
        "original_name": "The Flash",
        "overview":
            "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
        "popularity": 764.418,
        "poster_path": "/lJA2RCMfsWoskqlQhXPSLFQGXEJ.jpg",
        "seasons": [
          {
            "air_date": "2016-04-19",
            "episode_count": 8,
            "id": 79954,
            "name": "Specials",
            "overview": "",
            "poster_path": "/ft8pUr3qX41kOux5eqrwf02yAxZ.jpg",
            "season_number": 0
          }
        ],
        "status": "Returning Series",
        "tagline": "The fastest man alive.",
        "type": "Scripted",
        "vote_average": 7.8,
        "vote_count": 8687
      };
      expect(result, expectedJsonMap);
    });
  });
}
